from django.db import models

# Create your models here.


class X(models.Model):
    text = models.TextField(
	null=True,
	blank=True
    )
    
    def __unicode__(self):
	return self.text