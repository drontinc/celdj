from .models import X
from celery.decorators import task
from celery import shared_task

@shared_task
def new_x(txt):
    X(text=txt).save()
